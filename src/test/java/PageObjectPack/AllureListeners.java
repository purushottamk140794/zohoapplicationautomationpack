package PageObjectPack;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.qameta.allure.Attachment;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import utils.BaseClass;
import utils.ExtendReportNG;

public class AllureListeners implements ITestListener
{
    Logger log= Logger.getLogger(AllureListeners.class.getName());


    private  static String getTestMethodName(ITestResult result)
    {
        return result.getMethod().getConstructorOrMethod().getName();
    }
    @Attachment
    private  byte[] saveFailureScreenShot(WebDriver driver)
    {
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }
    @Attachment(value = "{0}",type = "text/plain")
    private  static String saveTextLog(String message)
    {
        return message;
    }

    @Override
    public void onTestStart(ITestResult iTestContext) {

        log.info("I am in OnStart Method "+iTestContext.getName());
        iTestContext.setAttribute("WebDriver", BaseClass.getDriver());

    }
    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        log.info("I am in onTestSuccess method " + getTestMethodName(iTestResult) + " succeed");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        log.info("I am in onTestFailure method" + getTestMethodName(iTestResult) + " succeed");
        Object testClass = iTestResult.getInstance();
        WebDriver driver = BaseClass.getDriver();
        // Allure ScreenShot and SaveTestLog
        if (driver instanceof WebDriver) {
            System.out.println("Screenshot captured for test case:" + getTestMethodName(iTestResult));
            saveFailureScreenShot(driver);
        }
        saveTextLog(getTestMethodName(iTestResult) + " failed and screenshot taken!");
    }
    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        log.info("I am in onTestSkipped method " + getTestMethodName(iTestResult) + " skipped");
    }
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        log.info("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
    }
    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        this.onTestFailure(result);
    }
    @Override
    public void onStart(ITestContext context) {
    }

    public void onFinish(ITestContext context) {
    }
}
