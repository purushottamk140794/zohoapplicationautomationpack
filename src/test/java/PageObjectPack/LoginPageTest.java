package PageObjectPack;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.LaunchPage;
import pages.LoginPage;
import utils.BaseClass;

import java.io.IOException;

//@Listeners({AllureListeners.class})
public class LoginPageTest extends BaseClass
{

    public WebDriver driver;
Logger log= Logger.getLogger(LoginPageTest.class.getName());


    @BeforeMethod
    public void initialize() throws IOException {
      PropertyConfigurator.configure("Log4j.properties");
            driver=initializeDriver();
        log.info("Driver is Initialize");


    }
        @Test()
        public void navigateToHomePage() throws IOException {
         PropertyConfigurator.configure("Log4j.properties");
            driver.get(properties.getProperty("url"));
            LaunchPage lp = new LaunchPage(driver);
            LoginPage loginPage = lp.getLogin();
            loginPage.enterUserName(credentials().split(":")[0]);
            loginPage.enterNext();
            loginPage.enterPassword(credentials().split(":")[1]);
            loginPage.pressSubmitBtn();

           log.info("TestCase successfully executed");
        }

       /* @DataProvider
        public Object[][] getData() {

            Object[][] data = new Object[1][2];
            data[0][0] = "practiceseleniumm@gmail.com";
            data[0][1] = "Practice#Selenium12";


            return data;
        }*/
@AfterMethod
    public void tearDown() {
    PropertyConfigurator.configure("Log4j.properties");
        driver.close();
        log.info("closing the browser");
    }

    }


