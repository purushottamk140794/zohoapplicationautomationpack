package PageObjectPack;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LaunchPage;
import utils.BaseClass;

import java.io.IOException;

public class LandingPageTest extends BaseClass {


    public WebDriver driver;
    Logger log= Logger.getLogger(LandingPageTest.class.getName());


    @BeforeMethod
    public void initialize() throws IOException {
        PropertyConfigurator.configure("Log4j.properties");
        driver=initializeDriver();
        log.info("Driver is Initialize");



    }
    @Test()
    public void validateOfLandingPage() throws IOException {
        driver.get(properties.getProperty("url"));
        LaunchPage lp=new LaunchPage(driver);
        PropertyConfigurator.configure("Log4j.properties");
        log.info("----validating of Customer text on Landing page---------");
        Assert.assertEquals(lp.getCustomers(),homePageValidation().split(":")[0],"Assertion fails");
        log.info(homePageValidation().split(":")[0]);

        log.info("----validating of Support text on Landing page---------");
        Assert.assertEquals(lp.getSupport(),homePageValidation().split(":")[1],"Assertion fails");
        log.info(homePageValidation().split(":")[1]);

        log.info("----validating of Contact Sales text on Landing page---------");
        Assert.assertEquals(lp.getContact(),homePageValidation().split(":")[2],"Assertion fails");
        log.info(homePageValidation().split(":")[2]);

        log.info("----validating of Sign Up text on Landing page---------");
        Assert.assertEquals(lp.getSignup(),homePageValidation().split(":")[3],"Assertion fails");
        log.info(homePageValidation().split(":")[3]);
    }


    @AfterMethod
    public void tearDown() {
        PropertyConfigurator.configure("Log4j.properties");
        driver.close();
        log.info("closing the browser");
    }
}
