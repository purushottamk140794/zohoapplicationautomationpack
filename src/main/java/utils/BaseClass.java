package utils;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {
    public WebDriver driver;
    public Properties properties = new Properties();
    public static ThreadLocal<WebDriver> tdriver =new ThreadLocal<WebDriver>();
    public WebDriver initializeDriver() throws IOException {
        properties = new Properties();
        FileInputStream fi = new FileInputStream(System.getProperty("user.dir") + "//src//main//java//utils//data.properties");
        properties.load(fi);
        String browserName = properties.getProperty("Browser");
        //String browserName = properties.getProperty("Browser");

        if (browserName.equalsIgnoreCase("Chrome")) {
            WebDriverManager.chromedriver().setup();
          //  System.setProperty("webdriver.crome.driver", System.getProperty("user.dir") + "//src//main//java//utils//chromedriver.exe");
            driver = new ChromeDriver();
        } else if (browserName.contains("Firefox")) {
           // System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//src//main//java//utils//geckodriver.exe");
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();

        } else if (browserName.equalsIgnoreCase("IE")) {
           // System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//src//main//java//utils//IEdriver.exe");
            WebDriverManager.iedriver().setup();
            driver = new InternetExplorerDriver();
        }
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        tdriver.set(driver);
        return getDriver();
    }
    public static synchronized WebDriver getDriver()
    {
        return tdriver.get();
    }

    public String getScreenshotPath(String testMethodName, WebDriver driver) throws IOException {
        TakesScreenshot ts = (TakesScreenshot) driver;
        //File source = ts.getScreenshotAs(OutputType.FILE);
        String destination = System.getProperty("user.dir") + "\\reports\\" + testMethodName + ".png";
        //FileUtils.copyFile(source, new File(destination));
       return destination;
    }
  protected String credentials() throws IOException {
      properties = new Properties();
      FileInputStream fi = new FileInputStream(System.getProperty("user.dir") + "//src//main//java//utils//data.properties");
      properties.load(fi);
      String userName = properties.getProperty("validUserName");
      String password=properties.getProperty("validPassword");
      return userName+":"+password;
  }
    protected String homePageValidation() throws IOException {
        properties = new Properties();
        FileInputStream fi = new FileInputStream(System.getProperty("user.dir") + "//src//main//java//utils//data.properties");
        properties.load(fi);
        String customer = properties.getProperty("customer");
        String support=properties.getProperty("support");
        String contact = properties.getProperty("contact");
        String signUp=properties.getProperty("signUp");
        return customer+":"+support+":"+contact+":"+signUp;
    }
}