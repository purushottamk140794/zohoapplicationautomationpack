package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LaunchPage {


    WebDriver driver;
    @FindBy(className = "zh-login")
    WebElement login;

    @FindBy(className = "zh-customers")
    WebElement customers;

    @FindBy(css = ".zh-support")
    WebElement support;

    @FindBy(xpath = "//a[contains(text(),'Contact Sales')]")
    WebElement contact;

    @FindBy(xpath = "//a[contains(text(),'Free Sign Up')]")
    WebElement signup;

    @FindBy(css = ".zgh-localization.init div ul")
    WebElement langauge;

    public LaunchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public LoginPage getLogin()
    {
        login.click();
        return new LoginPage(driver);
    }

    public  String getCustomers()
    {
        return customers.getText();
    }

    public String getSupport()
    {
        return support.getText();
    }

    public String getContact()
    {
        return contact.getText();
    }
    public String getSignup()
    {
        return signup.getText();
    }
}
