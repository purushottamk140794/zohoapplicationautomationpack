package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {

    @FindBy(className = "_logo-books _logo-x96 zod-app-logo")
    public WebElement books;

    @FindBy(className = "_logo-calendar _logo-x96 zod-app-logo")
    public WebElement calender;

    @FindBy(className = "_logo-crm _logo-x96 zod-app-logo")
    public WebElement crm;
}
