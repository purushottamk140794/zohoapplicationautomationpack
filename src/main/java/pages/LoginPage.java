package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    WebDriver driver;
    @FindBy(id = "login_id")
    WebElement userName;

    @FindBy(id = "nextbtn")
     WebElement next;

    @FindBy(css = "input[id='password']")
     WebElement password;

    @FindBy(xpath = "//button[@id='nextbtn']//span[contains(text(),'Sign in')]")
    WebElement submitBtn;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterUserName(String username)
    {
        userName.sendKeys(username);
    }
    public void enterNext()
    {
        next.click();
    }
    public void enterPassword(String pass)
    {
        password.sendKeys(pass);
    }
    public void pressSubmitBtn()
    {
        submitBtn.click();
    }

}
